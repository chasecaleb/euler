#encoding: utf-8
"""
PROBLEM
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

PSEUDOCODE
FUNCTION sequence (n, terms = 1)
	IF n == 1 THEN #base case
		return [n, terms]
	ELSE IF n % 2 == 0
		return sequence(n/2, terms + 1)
	ELSE
		return sequence(3n + 1, terms + 1)
	END IF
END FUNCTION

largest = [0,0] #list containing number,terms
FOR n equals one through one million
	IF sequence(n)[1] > largest[1] THEN:
		print sequence(n)
	END IF
END FOR
"""
#recursive version -- works, but slow
def sequence (n, length = 1):
	if n == 1:
		return length
	elif n % 2 == 0:
		return sequence(n / 2, length + 1)
	else:
		return sequence(n * 3 + 1, length + 1)

#new/improved/fast iterative version
def sequence_iter(n):
	length = 1
	while True:
		if n == 1:
			return length
		elif n % 2 == 0:
			n = n / 2
			length += 1
		else:
			n = n * 3 + 1
			length += 1

def recursive (x):
	largest = [0,0] #largest chain in form [number,length]
	for n in xrange(2, x + 1):
		if sequence(n) > largest[1]:
			largest = [n,sequence(n)]
	print largest

def iterative (x):
	largest = [0,0] #largest chain in form [number,length]
	for n in xrange(2, x + 1):
		if sequence_iter(n) > largest[1]:
			largest = [n,sequence_iter(n)]
	print largest

x = 9999
iterative(x) #33.7s
#recursive(x) #51.8s