#!/usr/bin/env runghc

sumOfSquares :: (Integral a) => [a] -> a
sumOfSquares = foldr (\ x -> (+) (x * x)) 0

squareOfSums :: (Integral a) => [a] -> a
squareOfSums xs = sum xs * sum xs

differenceSquareAndSum :: (Integral a) => [a] -> a
differenceSquareAndSum x = squareOfSums x - sumOfSquares x

main :: IO ()
main = print $ differenceSquareAndSum ([1..100] :: [Int])
