#!/usr/bin/env python3
import math

"""
Problem:
    215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
    What is the sum of the digits of the number 21000?

Solution:
    The number of paths from origin (0,0) to point (a,b) is given by the
    binomial coefficient (a + b) choose a. The value is (a + b)! / (a! * b!).
    Reference: http://mathworld.wolfram.com/LatticePath.html
"""

a, b = int(input("A: ")), int(input("B: "))
paths = math.factorial(a + b) / (math.factorial(a) * math.factorial(b))
print(paths)
