#!/usr/bin/env runghc

sumDigits :: Integral a => a -> a
sumDigits x = wrapped x 0
    where
        wrapped 0 acc = acc
        wrapped y acc = wrapped (y `div` 10) (acc + y `mod` 10)

main :: IO ()
main =
    print $ sumDigits $ product ([1..100] :: [Integer])
