"""
PROBLEM
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.

SOLUTION
Re-use code from problem 7 (below)

from math import sqrt
from math import trunc
def findPrime (nthTerm):
	term = 1
	x = 1
	#each iteration == one prime found
	while term < nthTerm:
		x += 2 #because primes are always odd
		factor = 2
		#max factor necessary to determine is x is prime
		#this massively optimizes calculation
		maxFactor = trunc(sqrt(x))
		isPrime = True
		#each iteration == one factor tried
		#determines if x is prime or not
		while factor <= maxFactor:
			if x % factor == 0:
				isPrime = False
				break
			else:
				factor += 1
		#x IS prime
		if isPrime == True:
			term += 1
	#nth prime found, return it
	return x

print findPrime(40001)
"""
from math import sqrt
from math import trunc

max = 2000000
sum = 2 #because 2 is prime, but won't be calculated by algorithm

for x in xrange(3, max, 2):
	maxFactor = trunc(sqrt(x))
	isPrime = True
	for factor in xrange(2, maxFactor + 1):
		if x % factor == 0:
			isPrime = False
			break
	if isPrime:
		if x <= max:
			sum += x
print sum

#if max = 200,000 then sum = 1709600813