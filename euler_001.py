"""
PROBLEM
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

SOLUTION
Instead of looping through every single int (1-1000) and dividing by both 3 and 5 (costly!), make lists of multiples of 3 and 5
Remove multiples of 15, due to duplicates

RECURSIVE METHOD (works, but overflows at 999)
def findMultiples (factor, max, sum = 0, acc = 1):
	if acc % factor == 0:
		if acc <= max:
			sum += acc
			return findMultiples(factor, max, sum, acc + 1)
		else:
			return sum
	else:
		return findMultiples(factor, max, sum, acc + 1)
"""
def findMultiples (factor, max, sum = 0, acc = 1):
	while acc < max:
		if acc % factor == 0:
				(factor, max, sum, acc) = (factor, max, sum + acc, acc + 1)
		else:
			(factor, max, sum, acc) = (factor, max, sum, acc + 1)
	return sum

print findMultiples(3, 1000) + findMultiples (5, 1000) - findMultiples(15, 1000)