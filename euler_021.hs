#!/usr/bin/env runghc

sumDivisors :: Integer -> Integer
sumDivisors x = sum (1 : divisors x 2)
    where
        divisors n i
            | i > floor (sqrt(fromInteger n) :: Double) = []
            | i^(2 :: Integer) == n = i : divisors n (i + 1)
            | n `mod` i == 0 = i : n `div` i : divisors n (i + 1)
            | otherwise = divisors n (i+1)

amicableNumbers :: Integer -> Integer
amicableNumbers = wrapped 0 0 0
    where
        wrapped y z acc x
            | y == x = acc
            | z == y = wrapped (y+1) y acc x
            | isNewPair = wrapped (y+1) (y+1) (acc + z + sumZ) x
            | otherwise = wrapped y (z+1) acc x
            where
                isNewPair = z > sumZ && z == sumDivisors sumZ && z /= sumZ
                sumZ = sumDivisors z

main :: IO ()
main = print $ amicableNumbers (50000 :: Integer)
