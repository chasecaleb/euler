"""
PROBLEM
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
What is the 10,001st prime number?

SOLUTION
Brute force checking up to sqrt(n)
"""
from math import sqrt
from math import trunc
def findPrime (nthTerm):
	term = 1
	x = 1
	#each iteration == one prime found
	while term < nthTerm:
		x += 2 #because primes are always odd
		factor = 2
		#max factor necessary to determine is x is prime
		#this massively optimizes calculation
		maxFactor = trunc(sqrt(x))
		isPrime = True
		#each iteration == one factor tried
		#determines if x is prime or not
		while factor <= maxFactor:
			if x % factor == 0:
				isPrime = False
				break
			else:
				factor += 1
		#x IS prime
		if isPrime == True:
			term += 1
	#nth prime found, return it
	return x

print(findPrime(10001))
