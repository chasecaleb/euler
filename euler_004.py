"""
PROBLEM
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit numbers.

SOLUTION
-Make list of all potential palindromes
	-Use set of two for loops for each factor
	-If/then statement to determine palindrome
		-if yes, add to array
-After all palindromes are found, determine highest one
	-output this

PSEUDOCODE
function isPalindrome (original):
	palindrome = reversed original (using strings?)
	if original == palindrome:
		append palindrome to palindromeList

function largestNumber (palindromeList):
	sort list by size
	return largest (last) in list

for x (1-999):
	for y (1-999):
		isPalindrome(x*y)
print largestNumber(palindromeList)
"""
palindromeList = []

def isPalindrome(original):
	palindrome = int(str(original)[::-1])
	if palindrome == original:
		palindromeList.append(original)

def largestNumber(xList):
	xList.sort()
	return xList[len(xList) - 1]

for x in xrange (100, 1000):
	for y in xrange(100, 1000):
		isPalindrome(x*y)
print largestNumber(palindromeList)