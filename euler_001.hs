#!/usr/bin/env runghc
import Data.List

{-
 If we list all the natural numbers below 10 that are multiples of 3 or 5,
 we get 3, 5, 6 and 9. The sum of these multiples is 23.

 Find the sum of all the multiples of 3 or 5 below 1000.
 -}

limit :: (Integral a) => a
limit = 1000 - 1
threes :: (Integral a) => [a]
threes = [ x | x <- [1..limit], x `mod` 3 == 0]
fives :: (Integral a) => [a]
fives = [x | x <- [1..limit], x `mod` 5 == 0]
threesAndFives :: (Integral a) => [a]
threesAndFives = threes `union` fives

main :: IO ()
main =
        print $ sum (threesAndFives :: [Int])
