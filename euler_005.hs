#!/usr/bin/env runghc

lcmRange :: (Integral a) => [a] -> a
lcmRange [] = 1
lcmRange [x] = x
lcmRange (x:xs) = lcm x (lcmRange xs)

main :: IO ()
main = print $ lcmRange ([1..20] :: [Int])
