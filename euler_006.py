"""
PROBLEM
The sum of the squares of the first ten natural numbers is,

1^2 + 2^2 + ... + 10^2 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)^2 = 55^2 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""

def squareOfSum (max, sum = 0):
	while max > 0:
		(max, sum) = (max - 1, sum + (max * max))
		print "sum: " + str(sum) + " max: " + str(max)
	return sum

def sumOfSquare(max, sum = 0):
	while max > 0:
		(max, sum) = (max - 1, sum + max)
	sum = sum * sum
	return sum


print "Difference is: " + str(sumOfSquare(100) - squareOfSum(100))