"""
PROBLEM
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a^2+ b^2 = c^2
For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

SOLUTION
simple/brute force method:
a < s/3
a < b < s/2

(REVISIT) Mathematical method:

Reference material:
	http://en.wikipedia.org/wiki/Pythagorean_triple
	http://www.mathblog.dk/pythagorean-triplets/

According to Euclid's formula,
While m and n are coprime (aka no common denominators) and m-n is odd:
	a = d(m^2 - n^2)
	b = 2d*m*n
	c = d(m^2 + n^2)

Therefore:
(s stands for sum of a + b + c, s = 1000 in this case)
s = 2m(m+n) + d

PSEUDOCODE
SET a to 1
SET b to 0
SET c to 0

FOR a up to s/3
	SET b to a
	FOR b up to s/2
		SET c to sum - (a + b)
		IF (a * a) + (b * b) == (c * c) THEN
			SOLUTION FOUND
			PRINT a * b * c
	END FOR
END FOR
"""
s = 1000
a = 1
b = 0
c = 0
count = 0

for a in xrange(s/3):
	b = a
	for b in xrange(s/2):
		count += 1
		c = s - (a + b)
		if (a * a) + (b * b) == (c * c):
			print a * b * c
			print "A: " + str(a) + " B: " + str(b) + " C: " + str(c)
			print "Iterations: " + str(count)