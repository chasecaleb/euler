#!/usr/bin/env python3
import csv
import string

"""
Compare this and the haskell solution.

Moral of the story: Python dominates at file/string parsing... or at least it
sure was quicker and easier.
"""

def get_names():
    with open("euler_022.txt", newline='') as f:
        names = []
        read = csv.reader(f, delimiter=',', quotechar='"')
        for row in read:
            for i in row:
                names.append(i)
    return names

def name_values(names):
    acc = 0
    for i, n in enumerate(names):
        stringValue = 0
        for c in n:
            stringValue += 1 + string.ascii_uppercase.index(c.upper())
        acc += (i+1) * stringValue
    return acc

def main():
    names = sorted(get_names())
    print(name_values(names))

if __name__ == "__main__":
    main()
