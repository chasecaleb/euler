#!/usr/bin/env runghc

-- This is so kludgy and naieve, but it works... for this problem, at
-- least.

firstInMonths :: (Integral a) => [a] -> a -> [a]
firstInMonths [] _ = []
firstInMonths (x:xs) acc = currentMonth : firstInMonths xs currentMonth
    where
        currentMonth = (x + acc) `mod` 7

firstInYears :: (Integral a) => [a] -> a -> [a]
firstInYears [] _ = []
firstInYears (x:xs) offset = currentYear ++ firstInYears xs (last currentYear)
    where
        currentYear
            | x `mod` 4 == 0 = firstInMonths leapMonths offset
            | otherwise = firstInMonths nonLeapMonths offset
        leapMonths = 31 : 29 : drop 2 nonLeapMonths
        nonLeapMonths = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

main :: IO ()
main =
    print $ length $ filter (== (0 :: Int)) (firstInYears [1901..2000] 2)
