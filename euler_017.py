#!/usr/bin/env python3

"""
Problem:
    If the numbers 1 to 5 are written out in words: one, two, three, four,
    five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

    If all the numbers from 1 to 1000 (one thousand) inclusive were written out
    in words, how many letters would be used?

    NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
    forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
    20 letters. The use of "and" when writing out numbers is in compliance with
    British usage.

Words/numbers:
    one, two, three, four, five, six, seven, eight, nine
    ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen,
        eighteen, nineteen
    twenty, twenty-one, twenty-two, twenty-three...
    one hundred, one hundred and one, one hundred and two...
    one hundred and ten, one hundred and eleven, one hundred and twelve...
    one hundred and twenty, one hundred and twenty-one...
"""

def number_to_word(number, digit_place=None, word=""):
    # Empty elements added for sanity (LOOKUP_ONES[1] == "one", etc)
    LOOKUP_ONES = ("", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen")
    LOOKUP_TENS = ("", "ten", "twenty", "thirty", "forty", "fifty", "sixty",
            "seventy", "eighty", "ninety")

    # Initialization
    number_str = str(number)
    if digit_place is None:
        digit_place = len(number_str)
    # Base case
    if digit_place == 0:
        return word

    digit = int(number_str[len(number_str) - digit_place])
    new_word = ""
    if digit_place == 4:
        new_word = LOOKUP_ONES[digit] + " thousand"
    elif digit_place == 3 and digit != 0:
        new_word = LOOKUP_ONES[digit] + " hundred"
        if number_str[-2:] != "00":
            new_word += " and"
    elif digit_place in (1, 2):
        if word and word[-1] != "-" and digit:
            word += " "

        if 10 < int(number_str[-2:]) < 20:
            # Looks up word representation of both digits (11, 12...19)
            # So only append the first time (do nothing if digit_place == 1)
            if digit_place == 2:
                new_word = LOOKUP_ONES[int(number_str[-2:])]
        elif digit_place == 2 and digit != 0:
            new_word = LOOKUP_TENS[digit]
            new_word += "-" if number_str[-1] != "0" else ""
        elif digit != 0:
            new_word = LOOKUP_ONES[digit]

    return number_to_word(number, digit_place - 1, word + new_word)

def main():
    length = 0
    for i in range(1, 1001):
        # Adding spaces/dashes only to strip was pointless but fun
        length += len(number_to_word(i).replace(" ", "").replace("-", ""))
    print(length)

if __name__ == "__main__":
    main()
