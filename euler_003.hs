#!/usr/bin/env runghc

factor :: (Integral a) => a -> a -> a
factor n i
        | n `mod` i == 0 = i
        | otherwise = factor n (i+1)

primeFactors :: (Integral a) => a -> [a]
primeFactors n
        | n == 1 = []
        | otherwise  = x : primeFactors (n `quot` x)
        where x = factor n 2

main :: IO ()
main = print $ last $ primeFactors (600851475143 :: Int)
