#!/usr/bin/env runghc
import Data.List

-- names function based on:
-- http://julipedia.meroh.net/2006/08/split-function-in-haskell.html
names :: String -> [String]
names c = filter (/="") $ (split . head . lines) c '"'
    where
        split [] _ = []
        split (x:xs) delim
            | x == delim = "" : rest
            | x == ',' = rest
            | otherwise = (x : head rest) : tail rest
            where
                rest = split xs delim

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = smaller ++ [x] ++ larger
    where smaller = quicksort (filter (<=x) xs)
          larger = quicksort (filter (>x) xs)

stringValue :: String -> Int
stringValue n = wrapped n 0
    where
        wrapped [] acc = acc
        wrapped (c:cs) acc = wrapped cs (acc + cValue)
            where cValue = 1 + maybe 0 (+0) (elemIndex c ['A'..'Z'])

nameValues :: [String] -> Int
nameValues ns = wrapped ns 0
    where
        wrapped [] acc = acc
        wrapped (x:xs) acc = wrapped xs (acc + value)
            where value = (1 + maybe 0 (+0) (elemIndex x ns)) * stringValue x

main :: IO ()
main = do
    contents <- readFile "euler_022.txt"
    print $ nameValues $ quicksort $ names contents
