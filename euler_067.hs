#!/usr/bin/env runghc

nestedList :: String -> [[Int]]
nestedList x = [map read $ words row :: [Int] | row <- rows]
    where rows = lines x

maxRow :: (Ord a, Num a) => [a] -> [a] -> [a]
maxRow x y = zipWith max fromLeft fromRight
    where
        fromLeft = zipWith (+) x y
        fromRight
            | length x > length y = zipWith (+) (drop 1 x) y
            | length y > length x = zipWith (+) x (drop 1 y)
            | otherwise = zipWith (+) x y

maxTriangle :: (Ord a, Num a) => [[a]] -> a
maxTriangle xs = head $ foldr1 maxRow xs

main :: IO ()
main = do
        contents <- readFile "euler_067.txt"
        print $ maxTriangle $ nestedList contents
