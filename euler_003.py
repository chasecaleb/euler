"""
PROBLEM
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?

SOLUTION
Factor number

-determine lowest factor of number
	-13195 % 2 != 0... 13195 % 5 == 0
	-5 is lowest factor of 13195
-determine lowest factor of new number (13195 / 5, aka 2639)
	-2639 % 7 == 0, new number is 2639 / 7 (377)
-repeat until no new numbers found
-return last number
"""

def findFactor(number):
	factor = 2
	while factor < number:
		if number % factor == 0:
			print factor
			number = number / factor
			factor = 2
		else:
			factor += 1
	return number

print "Greatest prime factor is: " + str(findFactor(600851475143))