"""
COMPARED TO PREVIOUS VERSION
Much faster this way (<1/2 run time)
Code is a total mess though, needs to be cleaned up... ew

PROBLEM
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

SOLUTION
Yes, v05.00 worked perfectly via brute force in increments of 2520 (lowest common multiple of 1-10).
However, that's (somewhat) cheating, although nonetheless elegant.
For a challenge, pretend that I don't know that, but use the same approach anyways.
In other words...
Determine LCM of 1 and 2 (2)
Determine LCM of 1, 2, and 3 starting from/incrementing by 2... 2? no. 4? no. 6? yes.
Determine LCM of 1, 2, 3, and 4 using same technique.... 6? no. 12? yes.
Keep working higher to 20.
Use some sort of fancy method of recursion to solve.
"""
solved = False

def findMultiple (factors, x):
	global solved
	multiple = x
	while solved == False:
		divisible = True
		for factor in factors:
			if multiple % factor != 0:
				divisible = False
		#if multiple matching all factors has been found
		if divisible == True:
			#if 1-20 have all been factored, end function/return multiple
			if factors[len(factors) - 1] == 20:
				print multiple
				solved = True
			#if not all factors have been used, add another and reiterate function
			#using new factor list and multiple
			else:
				#print str(multiple) + " is the LCM for: " + str(factors)
				newFactor = factors[len(factors) - 1] + 1
				factors.append(newFactor)
				findMultiple(factors, multiple)
		#if multiple does not work
		else:
			multiple += x

findMultiple([1, 2], 2)