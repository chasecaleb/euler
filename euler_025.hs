#!/usr/bin/env runghc

fib :: [Integer]
fib = 0 : 1 : zipWith (+) fib (tail fib)

main :: IO ()
main = do
    let n = 10^(999 :: Integer)
    print $ length $ takeWhile (<=n) fib
