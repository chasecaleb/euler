#!/usr/bin/env runghc

primes :: Integral a => [a]
primes = 2 : filter isPrime [3,5..]
    where
        isPrime x
            | x == 1 = False
            | otherwise = not $ any (\ n -> x `mod` n == 0) [2..endpoint x]
        endpoint x = floor $ (sqrt :: Double -> Double) $ fromIntegral (x-1)

nthPrime :: Int -> Int
nthPrime n = primes !! (n - 1)

main :: IO ()
main = print $ nthPrime 10001
