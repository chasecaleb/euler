#!/usr/bin/env runghc
import qualified Data.Set as Set

sumDivisors :: Integer -> Integer
sumDivisors x = sum (1 : divisors x 2)
    where
        divisors n i
            | i > floor (sqrt(fromInteger n) :: Double) = []
            | i^(2 :: Integer) == n = i : divisors n (i + 1)
            | n `mod` i == 0 = i : n `div` i : divisors n (i + 1)
            | otherwise = divisors n (i+1)

abundantNumbers :: [Integer]
abundantNumbers = filter (\ x -> x < sumDivisors x) [1,2..]

sumNonAbundant :: Integer -> Integer
sumNonAbundant n = sum [1..n] - Set.fold (+) 0 (Set.fromList $ abundantSums a a)
    where
        a = takeWhile (<n) abundantNumbers
        abundantSums [] _ = []
        abundantSums i [] = abundantSums (tail i) (tail i)
        abundantSums i j
            | value <= n = value : abundantSums i (tail j)
            | otherwise = abundantSums (tail i) (tail i)
            where
                value = head i + head j

main :: IO ()
main = (print . sumNonAbundant) 28123
