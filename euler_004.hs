#!/usr/bin/env runghc

palindromes :: [Integer]
palindromes = [i | x <- [100..999], y <- [x..999],
    let i = x*y, let s = show i, s == reverse s]

main :: IO ()
main = print $ maximum palindromes
